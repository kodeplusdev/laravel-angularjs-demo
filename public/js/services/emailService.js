// public/js/services/emailService.js

angular.module('emailService', [])

	.factory('Email', function ($http) {

		return {
			// get all the emails
			get: function () {
				return $http.get('/api/emails');
			},

			// save a email (pass in email data)
			save: function (emailData) {
				return $http({
					method: 'POST',
					url: '/api/emails',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param(emailData)
				});
			},

			// destroy a email
			destroy: function (id) {
				return $http.delete('/api/emails/' + id);
			}
		}

	});