// public/js/controllers/mainCtrl.js
angular.module('mainCtrl', [])

// inject the Email service into our controller
	.controller('mainController', function ($scope, $http, Email) {
		// object to hold all the data for the new email form
		$scope.emailData = {};

		// loading variable to show the spinning loading icon
		$scope.loading = true;

		// loading ckeditor config for body field
		$scope.editorOptions = {
			language: 'en',
			height: 120
		};

		// ===================== GET ALL EMAIL LOGS ===================== //
		// get all the emails first and bind it to the $scope.emails object
		// use the function we created in our service
		Email.get()
			.success(function (data) {
				$scope.emails = data;
				$scope.loading = false;
			});

		// ===================== SEND AND SAVE A EMAIL ===================== //
		// function to handle send and save a email
		$scope.sendEmail = function () {
			$scope.loading = true;

			// save the email. pass in email data from the form
			// use the function we created in our service
			Email.save($scope.emailData)
				.success(function (data) {
					$scope.emailData = {};
					// if successful, we'll need to refresh the email list
					Email.get()
						.success(function (getData) {
							$scope.emails = getData;
							$scope.loading = false;
						});
				})
				.error(function (data) {
					console.log(data);
				});
		};

		// ===================== DELETE A EMAIL ===================== //
		// function to handle deleting a email
		$scope.deleteEmail = function (id) {
			$scope.loading = true;

			// use the function we created in our service
			Email.destroy(id)
				.success(function (data) {
					// if successful, we'll need to refresh the email list
					Email.get()
						.success(function (getData) {
							$scope.emails = getData;
							$scope.loading = false;
						});
				});
		};
	});