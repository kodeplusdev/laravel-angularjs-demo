<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel and Angular Email System</title>

    <!-- CSS -->
    <link rel="stylesheet"
          href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- load bootstrap via cdn -->
    <link rel="stylesheet"
          href="bower_components/fontawesome/css/font-awesome.min.css">
    <!-- load fontawesome -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Dependencies -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/angular/angular.min.js"></script>
    <script src="bower_components/ckeditor/ckeditor.js"></script>
    <script src="bower_components/ng-ckeditor/ng-ckeditor.js"></script>

    <!-- ANGULAR -->
    <!-- load our controller -->
    <script src="js/controllers/mainCtrl.js"></script>
    <!-- load our service -->
    <script src="js/services/emailService.js"></script>
    <!-- load our application -->
    <script src="js/app.js"></script>
</head>
<!-- declare our angular app and controller -->
<body class="container" ng-app="emailApp" ng-controller="mainController">
<div class="col-md-8 col-md-offset-2">

    <!-- ====================== PAGE TITLE ====================== -->
    <div class="page-header">
        <h2>Laravel and Angular: Email System</h2>
    </div>

    <!-- ====================== EMAIL FORM ====================== -->
    <form ng-submit="sendEmail()">
        <!-- ng-submit will disable the default form action and use our function -->
        <div class="form-group">
            <input type="text" class="form-control input-sm" name="to"
                   ng-model="emailData.to" placeholder="To" required="true">
        </div>
        <div class="form-group">
            <input type="text" class="form-control input-sm" name="subject"
                   ng-model="emailData.subject" placeholder="Subject" required="true">
        </div>

        <!-- BODY TEXT -->
        <div class="form-group">
            <textarea class="form-control" name="body" id="emailBody"
                      ng-model="emailData.body"
                      placeholder="Body" rows="3"
                      ckeditor="editorOptions"></textarea>
        </div>

        <!-- SUBMIT BUTTON -->
        <div class="form-group text-right">
            <button type="submit" class="btn btn-primary"><span
                    class="glyphicon glyphicon-send"></span> Send
            </button>
        </div>
    </form>

    <h3>EMAIL LOGS</h3>
    <!-- ====================== LOADING ICON ====================== -->
    <!-- show loading icon if the loading variable is set to true -->
    <p class="text-center" ng-show="loading"><span
            class="fa fa-spinner fa-pulse fa-5x fa-fw"></span></p>

    <!-- ====================== THE EMAIL LOG ====================== -->
    <!-- hide these emails if the loading variable is true -->
    <div class="table-responsive">
        <table class="table table-striped table-bordered" ng-hide="loading">
            <thead>
            <tr>
                <th>Email #</th>
                <th>To</th>
                <th>Body</th>
                <th>Viewed</th>
                <th></th>
            </tr>
            </thead>
            <tr ng-repeat="email in emails">
                <th class="text-center">{{ email.id }}</th>
                <td>{{ email.to }}</td>
                <td>{{ email.body }}</td>
                <td class="text-right">{{ email.logs.length }}</td>
                <td class="text-center">
                    <button type="button" ng-click="deleteEmail(email.id)"
                            class="btn btn-danger btn-xs"><span
                            class="glyphicon glyphicon-trash"></span></button>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>