## Requirements
 - PHP >=5.4.0

## Setup
Step 1: Run a composer install on the command line from the root of your project (console)
```php
composer install
```
Step 2: Database Migration (console)
```php
php artisan migrate
```
Step 3: Run bower (console):
```php
cd public
bower install
```
Step 4: Update configuration on the following files:

```php
app/config/database.php
app/config/mail.php (required: from, username, password)
```