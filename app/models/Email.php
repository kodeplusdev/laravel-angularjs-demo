<?php

// app/models/Email.php

class Email extends Eloquent
{
// let eloquent know that these attributes will be available for mass assignment
    protected $fillable = array( 'to', 'subject', 'body');

    public function logs()
    {
        return $this->hasMany('EmailLog');
    }
}