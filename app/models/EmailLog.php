<?php

// app/models/Email.php

class EmailLog extends Eloquent
{
// let eloquent know that these attributes will be available for mass assignment
    protected $fillable = array('email_id');

    public function email()
    {
        return $this->belongsTo('Email');
    }
}