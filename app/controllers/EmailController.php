<?php // app/controllers/EmailController.php
class EmailController extends \BaseController {

    /**
     * Send back all emails as JSON
     *
     * @return Response
     */
    public function index()
    {
        $emails = Email::with('logs')
            ->select(DB::raw('emails.*, LEFT(emails.body, 150) as `body`, count(email_logs.id) as `log_count`'))
            ->leftjoin('email_logs', 'emails.id', '=', 'email_logs.email_id')
            ->orderBy('log_count', 'desc')
            ->groupBy('emails.id')
            ->limit(0)
            ->get();
        return Response::json($emails);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $subject = Input::get('subject');
        $body    = Input::get('body');
        $to      = Input::get('to');
        $email   = Email::create(array(
            'from'    => Input::get('from'),
            'to'      => $to,
            'subject' => $subject,
            'body'    => $body
        ));

        $data = array(
            'subject'  => $subject,
            'body'     => $body,
            'email_id' => $email->id,
            'to'       => $to
        );
        Mail::send('emails.mail',
            $data,
            function ($message) use ($data) {
                $message->to($data['to'], '')
                        ->subject($data['subject']);
            });

        return Response::json(array('success' => true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Email::destroy($id);

        return Response::json(array('success' => true));
    }

    /**
     * @param $id
     */
    public function anyLog($id)
    {
        header('Content-Type: image/gif');

        $email = Email::find($id);
        if ($email) {
            EmailLog::create(array(
                'email_id'    => $email->id,
            ));
        }

        $image = public_path() . '/img/blank.gif';
        //push out image
        if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off');	}
        header('Pragma: public'); 	// required
        header('Expires: 0');		// no cache
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private',false);
        header('Content-Disposition: attachment; filename="'.basename($image).'"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($image));	// provide file size
        readfile($image);		// push it out
        exit;
    }
} 